import Vue from "vue";
import VueTable from "./VueTable";

export const Components = {
  VueTable
};

Vue.component("VueTable", VueTable);
export default VueTable;
